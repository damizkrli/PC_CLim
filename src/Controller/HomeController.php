<?php

namespace App\Controller;

use App\Entity\Contact;
use App\Form\ContactType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     *
     * @throws TransportExceptionInterface
     */
    public function index(Request $request, MailerInterface $mailer): Response
    {
        $contact = new Contact();

        $form = $this->createForm(ContactType::class, $contact);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->addFlash('success', 'Votre message à bien été envoyé, '.$contact->getFirstName().' '.$contact->getLastName().
                '. Nous vous contacteront dans les plus bref délais.');

            $email = (new Email())
                ->from($contact->getEmail())
                ->to('pc-clim@hotmail.fr')
                ->subject('Vous avez reçu un message de la part de '.$contact->getFirstName().$contact->getLastName())
                ->html($this->renderView('notifications/newMessage.html.twig', [
                    'contact' => $contact,
                ]), 'utf-8');

            $mailer->send($email);

            return $this->redirect($this->generateUrl('home'));
        }

        return $this->render('home/index.html.twig', [
            'controller_name' => 'HomeController',
            'form' => $form->createView(),
        ]);
    }
}
