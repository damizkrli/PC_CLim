<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

class Contact
{
    /**
     * @var string|null
     * @Assert\NotBlank()
     * @Assert\Length(
     *     min=3,
     *     max=50,
     *     minMessage="{{ value }} est trop court. Veuillez saisir au moins {{ limit }} caractères.",
     *     maxMessage="{{ value }} est trop long. Veuillez saisir moins de {{ limit }} caractères."
     *     )
     */
    private $firstName;

    /**
     * @var string|null
     * @Assert\NotBlank()
     * @Assert\Length(
     *     min=3,
     *     max=50,
     *     minMessage="{{ value }} est trop court. Veuillez saisir au moins {{ limit }} caractères.",
     *     maxMessage="{{ value }} est trop long. Veuillez saisir {{ limit }} caractères maximum."
     * )
     */
    private $lastName;

    /**
     * @var string|null
     * @Assert\NotBlank()
     * @Assert\Email(
     *     message="{{ value }} n'est pas une adresse valide. Veuillez entrez une adresse Email valide."
     * )
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=15)
     * @Assert\NotBlank(
     *     message="Un numéro de téléphone est obligartoire."
     * )
     * @Assert\Length(
     *     max = 15,
     *     maxMessage = "Le numéro de téléphone ne doit pas faire plus de {{ limit }} caractères",
     * )
     */
    private $phoneNumber;

    /**
     * @var string|null
     * @Assert\NotBlank()
     * @Assert\Length(
     *     min=10,
     *     max=1000,
     *     minMessage="Votre message est trop court. Veuillez précisez votre demande.",
     *     maxMessage="Votre message est trop long. Il ne doit pas dépasser {{ limit }} lettres."
     * )
     */
    private $message;

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * @return $this
     */
    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    /**
     * @return $this
     */
    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @return $this
     */
    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->phoneNumber;
    }

    /**
     * @return $this
     */
    public function setPhoneNumber(string $phoneNumber): self
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    /**
     * @return $this
     */
    public function setMessage(string $message): self
    {
        $this->message = $message;

        return $this;
    }
}
